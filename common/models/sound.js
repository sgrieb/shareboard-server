'use strict';

var request = require('request');
var fs = require('fs');
var SOUND_LOCATION = process.env.SOUND_LOCATION;
var WEB_LOCATION = process.env.WEB_LOCATION;

module.exports = function(Sound) {

    Sound.remoteMethod (
        'addSound',
        {
          http: {path: '/addSound', verb: 'post'},
          accepts: [
              {arg: 'sound', type: 'Object'}
          ],
          returns: {arg: 'id', type: 'string'}
        }
    );

    Sound.addSound = function(data, cb) {
        // create the model so we have an id
        Sound.create(data, function(error, addedSound) {
            
            if (error) {
                cb(err.toString());
                return;
            }

            console.log('Trying to save at: ' + SOUND_LOCATION + addedSound.id.toString() + '.mp3');

            request('http://www.youtubeinmp3.com/fetch/?video=' + data.url) //+ '&start=' + data.start + '&end=' + data.end)
                .on('response', function(response) {
                    console.log(response.statusCode) // 200
                    console.log(response.headers['content-type']) // 'image/png'
                })
                .on('error', function(err) {
                    console.log(err);
                })
                .pipe(fs.createWriteStream(SOUND_LOCATION + addedSound.id.toString() + '.mp3')
                    .on('finish', function(event, listener) {
                        console.log('Successfully created sound ' + addedSound.id);
                        return cb(null, data);
                    })
                    .on('error', function(err) {
                        console.log('Error saving sound: ' + err);
                        return cb(err);
                    })
                );
                
        });
    };
};
